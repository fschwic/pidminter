var http = require('http');
var https = require('https');
var url = require('url');
var restify = require('restify');
var fs = require('fs');

var port = process.env.PORT || 8081;
var context = "pidminter";

var tmpDir = "/tmp/"
var currentPids = [];
var defaultPrefix;

function persistNextPid(prefix, suffix){
    var filename = tmpDir + prefix + ".json";
    fs.writeFile(filename, JSON.stringify(suffix), function(err) {
        if(err) {
            return console.log("Writing " + filename + ": " + err);
        }
        //console.log("Successfuly saved " + filename);
    });


function getPID (req, res, next){
    var count = 1;
    var prefix = defaultPrefix;
    if(req.query.prefix){
        if(currentPids[req.query.prefix]){
            prefix = req.query.prefix;
        }
        else{
            res.send(400, "Prefix " + req.query.prefix + " is not a registered prefix.");
            res.end();
        }
    }
    if(req.query.count){
        count = 0 + req.query.count;
    }

    var nextPid = prefix + "/" + currentPids[prefix].next;
    currentPids[prefix].next++;
    persistNextPid(prefix, currentPids[prefix].next);
    res.json(nextPid);
    res.end();
}

/* init */
var config = require('./config.json');
if(config.defaultPrefix){
    defaultPrefix = config.defaultPrefix;
}
if(config.tmpDir){
    tmpDir = config.tmpDir;
}
for (var pid in config.pid){
    currentPids[pid] = {};
    currentPids[pid].next = config.pid[pid].start;
    try{
        var savedNext = require(tmpDir + pid + ".json");
        if(savedNext){
            currentPids[pid].next = savedNext;
        }
    }
    catch(err){
        console.log("No next PID saved for prefix " + pid + ".");
    }
}

for (var cp in currentPids){
    console.log(cp + "/" + currentPids[cp].next);
}

/* configure and start server */
var server= restify.createServer();
server.use(restify.queryParser());
server.get('/favicon.ico', function(req, res, next){
  res.send(404);
  next();
});
server.get('/'+ context + "/pid", getPID);


server.listen(port, '0.0.0.0', function() {
    console.log('%s listening at %s', server.name, server.url);
});
console.log('Server running.');